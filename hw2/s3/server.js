const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});


//PUT
app.put('/products/:id', (req, res) =>{
   const id = req.params.id;
   let updated = false;
   products.forEach((product) =>{
      if(product.id == id){
          updated = true;
          product.productName=req.body.productName;
          product.price=req.body.price;
      } 
   });
   if(updated){
    res.status(200).send(`Produsul ${id} a fost actualizat!`);    
   } else {
       res.status(404).send(`Resursa ${id} inexistenta`);
   }
   
});


//DELETE
app.delete('/product-delete', (req, res) =>{
   const name = req.body.productName;
   products.forEach((product) =>{
   if(product.productName==name){
       const index=product.id;
       if(index<products.length)
        {products.splice(index,1);
        res.status(200).send(`Produsul ${name} a fost sters!`);
   }} else {
    res.status(404).send(`Produsul ${name} nu exista!`);
   }});
   
});


app.listen(8080, () => {
    console.log('Server started on port 8080...');
});
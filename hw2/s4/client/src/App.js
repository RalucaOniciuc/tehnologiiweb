import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

import {AddProduct} from './comp/AddProduct';
import {ProductList} from './comp/ProductList';

class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.products = [];
  }
  
  onProductAdded = (product) => {
    let products = this.state.products;
    products.push(product);
    this.setState({
      products: products
    });
  }
  
  componentWillMount(){
    const url = 'https://myworkspace-raluca31.c9users.io/get-all'
    fetch(url).then((res) => {
      return res.json();
    }).then((products) =>{
      this.setState({
         products: products
      })
    })
  }
  render() {
    return (
      <React.Fragment>
        <AddProduct productAdded={this.onProductAdded}/>
        <ProductList title="Product" source={this.state.products} />
      </React.Fragment>
    );
  }
}
export default App;